import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CaseItemModule } from '../../shared/case-item/case-item.module';
import { CaseSectionModule } from '../../shared/case-section/case-section.module';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing-module';

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule, CaseSectionModule, CaseItemModule],
})
export class HomeModule {}
