import { Component } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  public caseItems = [
    {
      bgImage: '/assets/img/case/man_1.png',
      image: '/assets/img/case/blue_case.png',
      name: 'soldier series',
      price: 250,
    },
    {
      bgImage: '/assets/img/case/man_2.png',
      image: '/assets/img/case/blue_case.png',
      name: 'soldier series',
      price: 250,
    },
  ];
}
