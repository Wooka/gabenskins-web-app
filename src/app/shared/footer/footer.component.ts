import { Component } from '@angular/core';

@Component({
  selector: 'g-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {}
