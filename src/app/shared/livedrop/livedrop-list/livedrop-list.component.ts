import { Component } from '@angular/core';

@Component({
  selector: 'g-livedrop-list',
  templateUrl: './livedrop-list.component.html',
  styleUrls: ['./livedrop-list.component.scss']
})
export class LivedropListComponent {
  public items = [
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
    {
      name: 'AWP',
      description: 'Chromatic Aberration',
      case: 'ST. PATRICK',
      img: '/assets/img/items/awp.png',
      rare: 'uncommon',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'ancient',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
    {
      name: 'AWP',
      description: 'Chromatic Aberration',
      case: 'ST. PATRICK',
      img: '/assets/img/items/awp.png',
      rare: 'uncommon',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'ancient',
    },
    {
      name: 'Glock-18',
      description: 'Wasteland Rebel',
      case: 'LIMITED CASE',
      img: '/assets/img/items/glock.png',
      rare: 'mythical',
    },
  ];
}
