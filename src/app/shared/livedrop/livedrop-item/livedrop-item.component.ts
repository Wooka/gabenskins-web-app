import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import * as moment from "moment";

moment.updateLocale('en', {
  relativeTime : {
    future: "in %s",
    past:   "%s ago",
    s  : '%d sec.',
    ss : '%d sec.',
    m:  "a min.",
    mm: "%d min.",
    h:  "an hour",
    hh: "%d hours",
    d:  "a day",
    dd: "%d days",
    w:  "a week",
    ww: "%d weeks",
    M:  "a month",
    MM: "%d months",
    y:  "a year",
    yy: "%d years"
  }
});

@Component({
  selector: 'g-livedrop-item',
  templateUrl: './livedrop-item.component.html',
  styleUrls: ['./livedrop-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LivedropItemComponent {
  @Input() rare:
    | 'common'
    | 'uncommon'
    | 'mythical'
    | 'legendary'
    | 'ancient'
    | string = 'common';
  @Input() caseName = '';
  @Input() time: Date = new Date();
  @Input() image = '';
  @Input() itemName = '';
  @Input() itemDescription = '';



  get timeFrom(): string {
    return moment(this.time).fromNow();
  }
}
