import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LivedropItemComponent } from './livedrop-item/livedrop-item.component';
import { LivedropListComponent } from './livedrop-list/livedrop-list.component';

@NgModule({
  declarations: [LivedropListComponent, LivedropItemComponent],
  imports: [CommonModule],
  exports: [LivedropListComponent],
})
export class LivedropModule {}
