import { Component, Input } from '@angular/core';

@Component({
  selector: 'g-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() text = '';
  @Input() uppercase = true;

  @Input() size: 'sm' | string = '';

  @Input() color: 'teal' | 'orange' = 'orange';
}
