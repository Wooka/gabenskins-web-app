import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CaseItemComponent } from './case-item.component';

@NgModule({
  declarations: [CaseItemComponent],
  imports: [CommonModule],
  exports: [CaseItemComponent],
})
export class CaseItemModule {}
