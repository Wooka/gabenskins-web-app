import { Component, Input } from '@angular/core';

import { ICaseItem } from './interface/case-item.interface';

@Component({
  selector: 'g-case-item',
  templateUrl: './case-item.component.html',
  styleUrls: ['./case-item.component.scss'],
})
export class CaseItemComponent {
  @Input() caseItem: ICaseItem | undefined;
}
