export interface ICaseItem {
  bgImage: string;
  image: string;
  name: string;
  price: number;
}
