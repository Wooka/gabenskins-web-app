import { Component, Input } from '@angular/core';

@Component({
  selector: 'g-case-section',
  templateUrl: './case-section.component.html',
  styleUrls: ['./case-section.component.scss'],
})
export class CaseSectionComponent {
  @Input() title = '';
  @Input() description = '';
}
