import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CaseSectionComponent } from './case-section.component';

@NgModule({
  declarations: [CaseSectionComponent],
  imports: [CommonModule],
  exports: [CaseSectionComponent],
})
export class CaseSectionModule {}
