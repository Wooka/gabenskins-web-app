import { Component } from '@angular/core';

@Component({
  selector: 'g-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public links = [
    {
      link: '',
      title: 'Provably fair',
      icon: 'shield',
    },
    {
      link: '',
      title: 'Promotional code',
      icon: 'promo',
    },
    {
      link: '',
      title: 'Affilate system',
      icon: 'affilate',
    },
  ];

}
